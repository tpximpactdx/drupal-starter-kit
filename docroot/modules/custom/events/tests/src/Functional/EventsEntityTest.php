<?php

namespace Drupal\Tests\events\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group events
 */
class EventsEntityTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'events',
  ];

  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin(
      $this->drupalCreateUser([
        'create event content',
        'delete any event content',
        'edit any event content',
      ])
    );

    $this->current_date = new \DateTime('2022-01-07');
    $this->term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
      ->loadByProperties(['name' => 'Music', 'vid' => 'event_types']);
    $this->term = reset($this->term);

    $this->edit = [];
    $this->edit['title[0][value]'] = $this->randomMachineName();
    $this->edit['body[0][value]'] = $this->randomMachineName(16);
    $this->edit['field_event_type'] = $this->term->id();
    $this->edit['field_date[0][value][date]'] = $this->current_date->format('Y-m-d');

    $this->owner = $this->createUser([], 'testuser');
    $this->nodeTitle = 'Event 1';
    $this->fieldDate = new \DateTime('2022-01-07');

    $this->node = $this->createNode([
      'title' => $this->nodeTitle,
      'type' => 'event',
      'uid' => $this->owner->id(),
      'field_date' => $this->fieldDate->format('Y-m-d'),
    ]);

  }

  /**
   * Tests creating a new node using node create form.
   *
   * @group events_create
   */
  public function testEventCreate() {
    $this->drupalGet('/node/add/event');
    $this->submitForm($this->edit, 'Save');

    $node = $this->drupalGetNodeByTitle($this->edit['title[0][value]']);
    $this->assertEquals($this->current_date->format('Y-m-d'), $node->get('field_date')->value);
    $node->delete();
  }

  /**
   * Tests deleting node using node delete form.
   *
   * @group events_delete
   */
  public function testEventDelete() {
    $node = $this->drupalGetNodeByTitle($this->node->label());
    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->submitForm([], 'Delete');



    $this->assertSession()->responseContains('The Event <em class="placeholder">' . $node->label() . '</em> has been deleted.');
  }
}
