<?php

namespace Drupal\Tests\events\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group events
 */
class EventsUrlsTest extends BrowserTestBase {

  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'events',
  ];


  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin(
      $this->drupalCreateUser(['access administration pages'])
    );
  }

  /**
   * Test description.
   *
   * @group events_url
   */
  public function testEventUrl() {
    $this->drupalGet('/events');
    $session = $this->assertSession();
    $session->statusCodeEquals(200);
  }
}
