<?php

namespace Drupal\Tests\events\Unit;

use Drupal\events\Controller\ContrivedController;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group events
 * @group events_controller
 */
class ContrivedControllerTest extends UnitTestCase {

  /**
   * Testing add method.
   *
   * @group events_add
   */
  public function testAdd() {
    // $controller = new ContrivedController();
    // $this->assertEquals(4, $controller->add(2, 2));

    $controller = $this->getMockBuilder(ContrivedController::class)
      ->disableOriginalConstructor()
      ->getMock();

    // Use reflection to make add() public.
    $ref_add = new \ReflectionMethod($controller, 'add');
    $ref_add->setAccessible(TRUE);

    $this->assertEquals(4, $ref_add->invokeArgs($controller, [2, 2]));

  }

}
