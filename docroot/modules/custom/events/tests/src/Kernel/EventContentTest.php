<?php

namespace Drupal\Tests\events\Kernel;

use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test description.
 *
 * @group events
 */
class EventContentTest extends KernelTestBase {

  // Additional traits can be imported for more prebuilt tools in the tests.
  use NodeCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'events',
    'field',
    'filter',
    'menu_ui',
    'node',
    'system',
    'taxonomy',
    'text',
    'user',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setUpEntity();

    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    $this->installSchema('user', ['users_data']);

    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('view');

    $this->installConfig(['events', 'filter', 'node', 'system', 'taxonomy', 'views']);

    $this->owner = $this->createUser([], 'testuser');

    $this->nodeTitle = 'Event 1';
    $this->fieldDate = new \DateTime('2022-01-07');

    $this->node = $this->createNode([
      'title' => $this->nodeTitle,
      'type' => 'event',
      'uid' => $this->owner->id(),
      'field_date' => $this->fieldDate->format('Y-m-d'),
    ]);

  }

  /**
   * Additional entity setup that doesn't get imported from site config.
   */
  protected function setUpEntity() {
    FieldStorageConfig::create([
      'field_name' => 'body',
      'entity_type' => 'node',
      'type' => 'text_with_summary',
      'cardinality' => 1,
    ])->save();

    EntityViewMode::create([
      'enabled' => TRUE,
      'id' => 'node.teaser',
      'status' => TRUE,
      'targetEntityType' => 'node',
    ])->save();
  }

  /**
   * Testing title field is there and working.
   *
   * @group events_kernel
   */
  public function testTitleField() {
    $this->assertEquals($this->nodeTitle, $this->node->getTitle());

    // Demonstrate first assertion fail stops running code.
    // $this->assertEquals($this->fieldDate->format('Y-m-d'), $this->node->get('field_date')->value);
  }

  /**
   * Testing date field.
   *
   * @group events_kernel
   */
  public function testDateField() {
    $this->assertEquals($this->fieldDate->format('Y-m-d'), $this->node->get('field_date')->value);
  }

}
