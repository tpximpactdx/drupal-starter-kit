<?php

namespace Drupal\events\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContrivedController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation')
    );
  }

  /**
   * Construct a new controller.
   */
  public function __construct(TranslationInterface $translation) {
    $this->setStringTranslation($translation);
  }

  /**
   * A controller method which displays a sum in terms of hands.
   */
  public function displayAddedNumbers($first, $second) {
    return [
      '#markup' => '<p>' . $this->handCount($first, $second) . '</p>',
    ];
  }

  /**
   * Generate a message based on how many hands are needed to count the sum.
   */
  protected function handCount($first, $second) {
    $sum = abs($this->add((int)$first, (int)$second));
    if ($sum <= 5) {
      $message = $this->t('I can count these on one hand.');
    }
    else if ($sum <= 10) {
      $message = $this->t('I need two hands to count these.');
    }
    else {
      $message = $this->t("That's just too many numbers to count.");
    }
    return $message;
  }

  /**
   * Add two numbers.
   */
  protected function add($first, $second) {
    return $first + $second;
  }

}
